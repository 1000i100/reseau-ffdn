const http = require('http');
const https = require('https');
const fs = require('fs-extra');
const yaml = require('js-yaml');

function url2jsonObj(options = {}) {
	return new Promise( (resolve, reject)=>{
		let req = (options.port===443?https:http).request(options, (response) => {
			let strJson = '';
			response.on('data', (chunk)=> strJson+=chunk );
			response.on('end', function () {
				resolve(JSON.parse(strJson));
			});
		});
		if(options.postData) req.write(options.postData);
		req.end();
	});
}
async function run() {
	const opt = {
		host:"db.ffdn.org",
		port:443,
		path:`/api/v1/isp/?per_page=9999`
	};
	const res = await url2jsonObj(opt);
	const allFai = [];
	res.isps.forEach((faiRaw)=>{
		const faiData = faiRaw.ispformat;
		const id = `node-fai-${faiData.shortname?norm(faiData.shortname):norm(faiData.name)}`;
		const label = faiData.shortname?faiData.shortname:faiData.name;
		const fai = {id:id,label:label,name:faiData.name};
		if(faiRaw.is_ffdn_member)fai.ffdnMember=faiRaw.is_ffdn_member;
		if(faiRaw.progressStatus)fai.progressStatus=faiData.progressStatus;
		if(faiData.website)fai.website=faiData.website;
		if(faiData.creationDate)fai.creationDate=faiData.creationDate;
		if(faiData.ffdnMemberSince)fai.ffdnMemberSince=faiData.ffdnMemberSince;
		if(faiData.description)fai.description=faiData.description;
		if(faiData.subscriberCount)fai.subscriberCount=faiData.subscriberCount;
		if(faiData.memberCount)fai.memberCount=faiData.memberCount;
		if(faiData.email)fai.email=faiData.email;
		if(faiData.registeredOffice && faiData.registeredOffice.locality)fai.locality=faiData.registeredOffice.locality;
		if(faiData.registeredOffice && faiData.registeredOffice.region)fai.region=faiData.registeredOffice.region;
		if(faiData.registeredOffice && faiData.registeredOffice["postal-code"])fai.postalCode=faiData.registeredOffice["postal-code"];
		if(faiData.registeredOffice && faiData.registeredOffice["country-name"])fai.country=faiData.registeredOffice["country-name"];
		if(faiData.coordinates && faiData.coordinates.longitude)fai.longitude=faiData.coordinates.longitude;
		if(faiData.coordinates && faiData.coordinates.latitude)fai.latitude=faiData.coordinates.latitude;
		if(faiData.logoURL)fai.logoURL=faiData.logoURL;
		allFai.push(fai);
	});
	await fs.writeFile('data.yml', yaml.safeDump({node:allFai}));
}
function norm(str){
	return str.replace(/[^A-Za-z0-9]/g,"").substr(0,20);
}
run();